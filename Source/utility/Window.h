#pragma once
#include <vector>
#include "Histogram.h" 

#include "../JuceLibraryCode/JuceHeader.h"

class Window
{
public:
    Window (int size);
    ~Window();

    float getNormalisedValue (float nextValue);

private:
    Window() {}
    Histogram histogram;
    std::vector<float> buffer;
    bool go                         = false;
    const int bufferSize            = 50;
    int bufferWritePos              = 0;
    float standardDeviation         = 0.f;
    float mean                      = 0.f;
    float min                       = 0.f;
    float max                       = 0.f;

    std::vector<float> distribution;
};
