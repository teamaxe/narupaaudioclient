#include "Histogram.h"
#include <algorithm>

Histogram::Histogram()
{
}

Histogram::~Histogram()
{
}

std::vector<float> Histogram::getBuckets (const std::vector<float>& source, int totalBuckets)
{
    auto min = *std::min_element (source.begin(), source.end());
    auto max = *std::max_element (source.begin(), source.end());

    std::vector<float> buckets (totalBuckets, 0);

    auto bucketSize = (max - min) / totalBuckets;

    for (auto& value : source)
    {
        auto bucketIndex = 0;
        if (bucketSize > 0.0)
        {
            bucketIndex = (int)((value - min) / bucketSize);
            if (bucketIndex == totalBuckets)
                bucketIndex--;
        }
        buckets[bucketIndex]++;
    }
    return buckets;
}

