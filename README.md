# NarupaAudioClient - Currently Windows Only

####  1. Install gRPC dependencies

* see [here](https://github.com/grpc/grpc/blob/master/BUILDING.md)

#### 2. Clone this repository
`git clone --recurse-submodules https://bitbucket.org/teamaxe/narupaaudioclient.git`

#### 3. Build the NarupaClient project
* create a build folder in `narupacppclient` and run cmake (your VS version or architecture may vary):
```
mkdir narupacppclient\build
cd narupacppclient\build
cmake -G "Visual Studio 16 2019" -A x64 ..
```
The last command will take some time as it downloads some dependencies

#### 4. Building the Client / Audio Plugin

* Open `NarupaAudioClient.jucer` in the Projucer - if you don't have JUCE installed then get it [here](https://github.com/WeAreROLI/JUCE) - build and run the Projucer (located in the Juce `extras` folder)

* Save and open the NarupaAudioClient visual studio solution
* Add the narupaclientlib as an additional solution:
* In visual studio right click the NarupaAudioClient solution, add -> existing project
	- in the dropdown menu select "Solution Files (*.sln)"
	- navigate to and select `\narupacppclient\build\NarupaClient.sln` (this may take a minute)
* For the `AudioClient_SharedCode`, `AudioClient_StandalonePlugin` and `AudioClient_UnityPlugin` targets, right click references -> add references and tick:
	- `NarupaClientLib`

Cross those fingers and build
